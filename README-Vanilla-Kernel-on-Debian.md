## Dependencies

```
sudo ./setup.sh
```

## Fetch Source

```
./fetch.sh
```

## Configure

Import and update the configuration of your current kernel.

```
cd linux
cp /boot/config-$(uname -r) .config
make CC=/usr/lib/ccache/gcc -j$(nproc) olddefconfig
```

> On Debian, the `.config` of a kernel will contain a path to a `.pem` file
> that is used to sign the kernel. To the best of my knowledge, this file
> will never be made available as it is used by Debian maintainers to sign
> a kernel.
>
> Hence we have to remove the path to the `.pem` file from the kernel configuration.
> If we do not remove it, the build will fail.
>
> ```
> sed -i -E 's/(CONFIG_SYSTEM_TRUSTED_KEYS)=.*/\1=""/' .config
> ```

Optionally, run `menuconfig` to adjust kernel options.

```
make CC=/usr/lib/ccache/gcc -j$(nproc) menuconfig
```

## Build

```
make CC=/usr/lib/ccache/gcc -j$(nproc) deb-pkg
```

## Install

```
sudo dpkg -i ../*.deb
```
