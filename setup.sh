#!/bin/sh
set -e
set -u
pkgs=""
pkgs="$pkgs build-essential bc libncurses5-dev libelf-dev libssl-dev bison flex rsync"
pkgs="$pkgs ccache git kmod debhelper"
apt-get update && apt-get -q install -y  $pkgs
